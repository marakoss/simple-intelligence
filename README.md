# Simple Intelligence Monorepo

## Requirements

* Docker

## Documentation
Read more about Docker and Docker compose at [Docker](https://docs.docker.com/) website.

## Installation
1. Check whether you have docker installed, up and running.
2. We need to build the containers
	```
	docker-compose build
	```
	You can also supply the name for the services. Read more at ``docker-compose --help``
3. Once the base images are downloaded and our containers are built, we can start the services 
	```
	docker-compose up
	```
	make sure any other servers are not running at ports 80, and 3306 - you can remap those ports in `./docker-compose.yml`

## Usage
- Once the servers are running inside docker you can navigate to ``localhost:80`` to see API platform GraphQL documentation
- You might need to execute commands inside PHP container (for example symfony `console` command). 
	List all running containers by - to get the name of the service
	```
	docker ps
	```
	Then you can attach to the interactive terminal session of that service by:
	```
	docker exec -it name_of_service /bin/sh
	```
