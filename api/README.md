# Simple Intelligence API

API Platform Symfony PHP application

## Requirements

* PHP 7.4+
* Composer
* MySQL/MariaDB

## Documentation
Read more about API platform at [API Platform](https://api-platform.com) website.

## Usage
For development application is running in docker.
How to work with docker containers is described in top most README.md file in this repository. 

## Installation
1. While in the docker container (or if running on your local machine) run
	```
	composer install
	```
2. Update your database schema -> simply by running migrations -> again from inside your docker machine
	```
	./bin/console d:m:m
	```
	or you can run it directly as ``docker exec --it simple-intelligence-api_php_1 ./bin/console d:m:m``

## Development

#### scripts

- `composer lint` - zkontroluje, zda kód splňuje kódovací standardy
- `composer lint-fix` - zkontroluje a následně zkusí opravit kódovací standardy