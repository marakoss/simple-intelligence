<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210212110400 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE entities (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE items (id INT AUTO_INCREMENT NOT NULL, entity_type_id INT NOT NULL, name VARCHAR(255) NOT NULL, create_date_time DATETIME DEFAULT CURRENT_TIMESTAMP, INDEX IDX_E11EE94D5681BEB0 (entity_type_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE item_tag (item_id INT NOT NULL, tag_id INT NOT NULL, INDEX IDX_E49CCCB1126F525E (item_id), INDEX IDX_E49CCCB1BAD26311 (tag_id), PRIMARY KEY(item_id, tag_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE notes (id INT AUTO_INCREMENT NOT NULL, item_id INT NOT NULL, note LONGTEXT NOT NULL, create_date_time DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, INDEX IDX_11BA68C126F525E (item_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ratings (id INT AUTO_INCREMENT NOT NULL, item_id INT NOT NULL, name VARCHAR(255) NOT NULL, value SMALLINT NOT NULL, INDEX IDX_CEB607C9126F525E (item_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE relation_types (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE relations (id INT AUTO_INCREMENT NOT NULL, from_id INT DEFAULT NULL, to_id INT DEFAULT NULL, relation_type_id INT DEFAULT NULL, active TINYINT(1) DEFAULT \'1\' NOT NULL, create_date_time DATETIME DEFAULT CURRENT_TIMESTAMP, deactive_time DATETIME DEFAULT NULL, INDEX IDX_146CBF7878CED90B (from_id), INDEX IDX_146CBF7830354A65 (to_id), INDEX IDX_146CBF78DC379EE2 (relation_type_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tags (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE items ADD CONSTRAINT FK_E11EE94D5681BEB0 FOREIGN KEY (entity_type_id) REFERENCES entities (id)');
        $this->addSql('ALTER TABLE item_tag ADD CONSTRAINT FK_E49CCCB1126F525E FOREIGN KEY (item_id) REFERENCES items (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE item_tag ADD CONSTRAINT FK_E49CCCB1BAD26311 FOREIGN KEY (tag_id) REFERENCES tags (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE notes ADD CONSTRAINT FK_11BA68C126F525E FOREIGN KEY (item_id) REFERENCES items (id)');
        $this->addSql('ALTER TABLE ratings ADD CONSTRAINT FK_CEB607C9126F525E FOREIGN KEY (item_id) REFERENCES items (id)');
        $this->addSql('ALTER TABLE relations ADD CONSTRAINT FK_146CBF7878CED90B FOREIGN KEY (from_id) REFERENCES items (id)');
        $this->addSql('ALTER TABLE relations ADD CONSTRAINT FK_146CBF7830354A65 FOREIGN KEY (to_id) REFERENCES items (id)');
        $this->addSql('ALTER TABLE relations ADD CONSTRAINT FK_146CBF78DC379EE2 FOREIGN KEY (relation_type_id) REFERENCES relation_types (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE items DROP FOREIGN KEY FK_E11EE94D5681BEB0');
        $this->addSql('ALTER TABLE item_tag DROP FOREIGN KEY FK_E49CCCB1126F525E');
        $this->addSql('ALTER TABLE notes DROP FOREIGN KEY FK_11BA68C126F525E');
        $this->addSql('ALTER TABLE ratings DROP FOREIGN KEY FK_CEB607C9126F525E');
        $this->addSql('ALTER TABLE relations DROP FOREIGN KEY FK_146CBF7878CED90B');
        $this->addSql('ALTER TABLE relations DROP FOREIGN KEY FK_146CBF7830354A65');
        $this->addSql('ALTER TABLE relations DROP FOREIGN KEY FK_146CBF78DC379EE2');
        $this->addSql('ALTER TABLE item_tag DROP FOREIGN KEY FK_E49CCCB1BAD26311');
        $this->addSql('DROP TABLE entities');
        $this->addSql('DROP TABLE items');
        $this->addSql('DROP TABLE item_tag');
        $this->addSql('DROP TABLE notes');
        $this->addSql('DROP TABLE ratings');
        $this->addSql('DROP TABLE relation_types');
        $this->addSql('DROP TABLE relations');
        $this->addSql('DROP TABLE tags');
    }
}
