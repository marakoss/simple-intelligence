<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210301232916 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Renamed Items=>Subjects, Entities=>Categories, Relations=>Connections, RelationTypes=>ConnectionTypes';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE items DROP FOREIGN KEY FK_E11EE94D5681BEB0');
        $this->addSql('ALTER TABLE item_tag DROP FOREIGN KEY FK_E49CCCB1126F525E');
        $this->addSql('ALTER TABLE notes DROP FOREIGN KEY FK_11BA68C126F525E');
        $this->addSql('ALTER TABLE ratings DROP FOREIGN KEY FK_CEB607C9126F525E');
        $this->addSql('ALTER TABLE relations DROP FOREIGN KEY FK_146CBF7830354A65');
        $this->addSql('ALTER TABLE relations DROP FOREIGN KEY FK_146CBF7878CED90B');
        $this->addSql('ALTER TABLE relations DROP FOREIGN KEY FK_146CBF78DC379EE2');
        $this->addSql('CREATE TABLE categories (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE subjects (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, created_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, INDEX search_idx (name), INDEX id_ix (id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE subject_category (subject_id INT NOT NULL, category_id INT NOT NULL, INDEX IDX_1EDB0CAC979B1AD6 (subject_id), INDEX IDX_1EDB0CAC12469DE2 (category_id), PRIMARY KEY(subject_id, category_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE subject_tag (subject_id INT NOT NULL, tag_id INT NOT NULL, INDEX IDX_77A33EB979B1AD6 (subject_id), INDEX IDX_77A33EBBAD26311 (tag_id), PRIMARY KEY(subject_id, tag_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE connection_types (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE connections (id INT AUTO_INCREMENT NOT NULL, from_id INT DEFAULT NULL, to_id INT DEFAULT NULL, connection_type_id INT DEFAULT NULL, active TINYINT(1) DEFAULT \'1\' NOT NULL, created_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, disabled_at DATETIME DEFAULT NULL, INDEX IDX_BFF6FC1578CED90B (from_id), INDEX IDX_BFF6FC1530354A65 (to_id), INDEX IDX_BFF6FC15BE466AB0 (connection_type_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE subject_category ADD CONSTRAINT FK_1EDB0CAC979B1AD6 FOREIGN KEY (subject_id) REFERENCES subjects (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE subject_category ADD CONSTRAINT FK_1EDB0CAC12469DE2 FOREIGN KEY (category_id) REFERENCES categories (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE subject_tag ADD CONSTRAINT FK_77A33EB979B1AD6 FOREIGN KEY (subject_id) REFERENCES subjects (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE subject_tag ADD CONSTRAINT FK_77A33EBBAD26311 FOREIGN KEY (tag_id) REFERENCES tags (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE connections ADD CONSTRAINT FK_BFF6FC1578CED90B FOREIGN KEY (from_id) REFERENCES subjects (id)');
        $this->addSql('ALTER TABLE connections ADD CONSTRAINT FK_BFF6FC1530354A65 FOREIGN KEY (to_id) REFERENCES subjects (id)');
        $this->addSql('ALTER TABLE connections ADD CONSTRAINT FK_BFF6FC15BE466AB0 FOREIGN KEY (connection_type_id) REFERENCES connection_types (id)');
        $this->addSql('DROP TABLE entities');
        $this->addSql('DROP TABLE item_tag');
        $this->addSql('DROP TABLE items');
        $this->addSql('DROP TABLE relation_types');
        $this->addSql('DROP TABLE relations');
        $this->addSql('DROP INDEX IDX_11BA68C126F525E ON notes');
        $this->addSql('ALTER TABLE notes CHANGE item_id subject_id INT NOT NULL, CHANGE create_date_time created_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL');
        $this->addSql('ALTER TABLE notes ADD CONSTRAINT FK_11BA68C979B1AD6 FOREIGN KEY (subject_id) REFERENCES subjects (id)');
        $this->addSql('CREATE INDEX IDX_11BA68C979B1AD6 ON notes (subject_id)');
        $this->addSql('DROP INDEX IDX_CEB607C9126F525E ON ratings');
        $this->addSql('ALTER TABLE ratings CHANGE item_id subject_id INT NOT NULL');
        $this->addSql('ALTER TABLE ratings ADD CONSTRAINT FK_CEB607C9979B1AD6 FOREIGN KEY (subject_id) REFERENCES subjects (id)');
        $this->addSql('CREATE INDEX IDX_CEB607C9979B1AD6 ON ratings (subject_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE subject_category DROP FOREIGN KEY FK_1EDB0CAC12469DE2');
        $this->addSql('ALTER TABLE subject_category DROP FOREIGN KEY FK_1EDB0CAC979B1AD6');
        $this->addSql('ALTER TABLE subject_tag DROP FOREIGN KEY FK_77A33EB979B1AD6');
        $this->addSql('ALTER TABLE connections DROP FOREIGN KEY FK_BFF6FC1578CED90B');
        $this->addSql('ALTER TABLE connections DROP FOREIGN KEY FK_BFF6FC1530354A65');
        $this->addSql('ALTER TABLE notes DROP FOREIGN KEY FK_11BA68C979B1AD6');
        $this->addSql('ALTER TABLE ratings DROP FOREIGN KEY FK_CEB607C9979B1AD6');
        $this->addSql('ALTER TABLE connections DROP FOREIGN KEY FK_BFF6FC15BE466AB0');
        $this->addSql('CREATE TABLE entities (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE item_tag (item_id INT NOT NULL, tag_id INT NOT NULL, INDEX IDX_E49CCCB1126F525E (item_id), INDEX IDX_E49CCCB1BAD26311 (tag_id), PRIMARY KEY(item_id, tag_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE items (id INT AUTO_INCREMENT NOT NULL, entity_type_id INT NOT NULL, name VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, create_date_time DATETIME DEFAULT CURRENT_TIMESTAMP, INDEX id_ix (id), INDEX IDX_E11EE94D5681BEB0 (entity_type_id), INDEX search_idx (name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE relation_types (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE relations (id INT AUTO_INCREMENT NOT NULL, from_id INT DEFAULT NULL, to_id INT DEFAULT NULL, relation_type_id INT DEFAULT NULL, active TINYINT(1) DEFAULT \'1\' NOT NULL, create_date_time DATETIME DEFAULT CURRENT_TIMESTAMP, deactive_time DATETIME DEFAULT NULL, INDEX IDX_146CBF7830354A65 (to_id), INDEX IDX_146CBF7878CED90B (from_id), INDEX IDX_146CBF78DC379EE2 (relation_type_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE item_tag ADD CONSTRAINT FK_E49CCCB1126F525E FOREIGN KEY (item_id) REFERENCES items (id) ON UPDATE NO ACTION ON DELETE CASCADE');
        $this->addSql('ALTER TABLE item_tag ADD CONSTRAINT FK_E49CCCB1BAD26311 FOREIGN KEY (tag_id) REFERENCES tags (id) ON UPDATE NO ACTION ON DELETE CASCADE');
        $this->addSql('ALTER TABLE items ADD CONSTRAINT FK_E11EE94D5681BEB0 FOREIGN KEY (entity_type_id) REFERENCES entities (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE relations ADD CONSTRAINT FK_146CBF7830354A65 FOREIGN KEY (to_id) REFERENCES items (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE relations ADD CONSTRAINT FK_146CBF7878CED90B FOREIGN KEY (from_id) REFERENCES items (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE relations ADD CONSTRAINT FK_146CBF78DC379EE2 FOREIGN KEY (relation_type_id) REFERENCES relation_types (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('DROP TABLE categories');
        $this->addSql('DROP TABLE subjects');
        $this->addSql('DROP TABLE subject_category');
        $this->addSql('DROP TABLE subject_tag');
        $this->addSql('DROP TABLE connection_types');
        $this->addSql('DROP TABLE connections');
        $this->addSql('DROP INDEX IDX_11BA68C979B1AD6 ON notes');
        $this->addSql('ALTER TABLE notes CHANGE subject_id item_id INT NOT NULL, CHANGE created_at create_date_time DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL');
        $this->addSql('ALTER TABLE notes ADD CONSTRAINT FK_11BA68C126F525E FOREIGN KEY (item_id) REFERENCES items (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_11BA68C126F525E ON notes (item_id)');
        $this->addSql('DROP INDEX IDX_CEB607C9979B1AD6 ON ratings');
        $this->addSql('ALTER TABLE ratings CHANGE subject_id item_id INT NOT NULL');
        $this->addSql('ALTER TABLE ratings ADD CONSTRAINT FK_CEB607C9126F525E FOREIGN KEY (item_id) REFERENCES items (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_CEB607C9126F525E ON ratings (item_id)');
    }
}
