<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210303081354 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Change IDs to UUIDs';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE categories CHANGE id id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\'');
        $this->addSql('ALTER TABLE subjects CHANGE id id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\'');
        $this->addSql('ALTER TABLE subject_category CHANGE subject_id subject_id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', CHANGE category_id category_id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\'');
        $this->addSql('ALTER TABLE subject_tag CHANGE subject_id subject_id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', CHANGE tag_id tag_id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\'');
        $this->addSql('ALTER TABLE connection_types CHANGE id id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\'');
        $this->addSql('ALTER TABLE connections CHANGE id id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', CHANGE from_id from_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:uuid)\', CHANGE to_id to_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:uuid)\', CHANGE connection_type_id connection_type_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:uuid)\'');
        $this->addSql('ALTER TABLE notes CHANGE id id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', CHANGE subject_id subject_id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\'');
        $this->addSql('ALTER TABLE ratings CHANGE id id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', CHANGE subject_id subject_id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\'');
        $this->addSql('ALTER TABLE tags CHANGE id id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\'');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE categories CHANGE id id INT AUTO_INCREMENT NOT NULL');
        $this->addSql('ALTER TABLE subjects CHANGE id id INT AUTO_INCREMENT NOT NULL');
        $this->addSql('ALTER TABLE subject_category CHANGE subject_id subject_id INT NOT NULL, CHANGE category_id category_id INT NOT NULL');
        $this->addSql('ALTER TABLE subject_tag CHANGE subject_id subject_id INT NOT NULL, CHANGE tag_id tag_id INT NOT NULL');
        $this->addSql('ALTER TABLE connection_types CHANGE id id INT AUTO_INCREMENT NOT NULL');
        $this->addSql('ALTER TABLE connections CHANGE id id INT AUTO_INCREMENT NOT NULL, CHANGE from_id from_id INT DEFAULT NULL, CHANGE to_id to_id INT DEFAULT NULL, CHANGE connection_type_id connection_type_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE notes CHANGE id id INT AUTO_INCREMENT NOT NULL, CHANGE subject_id subject_id INT NOT NULL');
        $this->addSql('ALTER TABLE ratings CHANGE id id INT AUTO_INCREMENT NOT NULL, CHANGE subject_id subject_id INT NOT NULL');
        $this->addSql('ALTER TABLE tags CHANGE id id INT AUTO_INCREMENT NOT NULL');
    }
}
