<?php

declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Doctrine\UuidGenerator;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     normalizationContext={"groups"={"connection"}},
 *     attributes={
 *          "pagination_via_cursor"
 *     },
 *     graphql={
 *          "item_query",
 *          "collection_query"={
 *              "pagination_via_cursor"
 *          },
 *          "delete",
 *          "update",
 *          "create"
 *     }, collectionOperations={
 *          "get",
 *          "post",
 *     }, itemOperations={
 *          "get",
 *          "put",
 *          "delete",
 *     }
 * )
 * @ORM\Entity
 * @ORM\Table(name="connections")
 */
class Connection
{
	/**
	 * @Groups({"subject", "connection"})
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="CUSTOM")
	 * @ORM\CustomIdGenerator(class=UuidGenerator::class)
	 * @ORM\Column(type="uuid", unique=true)
	 */
	private string $id;

	/**
	 * @Groups({"connection"})
	 * @ORM\ManyToOne(targetEntity=Subject::class, inversedBy="connectionOwners", fetch="EAGER")
	 */
	private Subject $from;

	/**
	 * @Groups({"connection"})
	 * @ORM\ManyToOne(targetEntity=Subject::class, inversedBy="connectionTargets", fetch="EAGER")
	 */
	private Subject $to;

	/**
	 * @Groups({"subject", "connection"})
	 * @ORM\Column(type="boolean", options={"default": "1"})
	 */
	private bool $active = true;

	/**
	 * @Groups({"subject", "connection"})
	 * @ORM\Column(type="datetime", options={"default": "CURRENT_TIMESTAMP"})
	 */
	private DateTimeInterface $createdAt;

	/**
	 * @Groups({"subject"})
	 * @ORM\Column(type="datetime", nullable=true)
	 */
	private ?DateTimeInterface $disabledAt = null;

	/**
	 * @Groups({"subject", "connection"})
	 * @ORM\ManyToOne(targetEntity=ConnectionType::class, inversedBy="connections")
	 */
	private ConnectionType $connectionType;

	public function __construct()
	{
		$this->createdAt = new \DateTimeImmutable();
	}

	public function getId(): ?string
	{
		return $this->id;
	}

	public function getActive(): ?bool
	{
		return $this->active;
	}

	public function setActive(bool $active): self
	{
		$this->active = $active;

		return $this;
	}

	public function getCreatedAt(): ?DateTimeInterface
	{
		return $this->createdAt;
	}

	public function getDisabledAt(): ?DateTimeInterface
	{
		return $this->disabledAt;
	}

	public function setDisabledAt(DateTimeInterface $time): self
	{
		$this->disabledAt = $time;

		return $this;
	}

	public function getConnectionType(): ConnectionType
	{
		return $this->connectionType;
	}

	public function setConnectionType(ConnectionType $connectionType): self
	{
		$this->connectionType = $connectionType;

		return $this;
	}

	public function getFrom(): Subject
	{
		return $this->from;
	}

	public function setFrom(Subject $from): void
	{
		$this->from = $from;
	}

	public function getTo(): Subject
	{
		return $this->to;
	}

	public function setTo(Subject $to): void
	{
		$this->to = $to;
	}
}
