<?php

declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Doctrine\UuidGenerator;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     normalizationContext={"groups"={"connectionType"}},
 *     graphql={
 *            "item_query",
 *            "collection_query"={
 *                "filters"={
 *                    "item.search_name_id_filter"
 *                }
 *            },
 *            "create"
 *        }, collectionOperations={
 *            "get",
 *            "post",
 *        }, itemOperations={
 *            "get",
 *            "delete",
 *            "put",
 *        }
 * )
 * @ORM\Entity
 * @ORM\Table(name="connection_types")
 *
 * Vzhledem k tomu, ze o relacnich typech neni v navrhu temer zadna zminka,
 * tak CREATE, UPDATE ani DELETE zde definovany nebudou.
 * Typ relace bude predem dane staticke pole s predefinovanymi hodnotami
 * --> tim padem je zde ApiResource naprosto zbytecne.
 */
class ConnectionType
{
	/**
	 * @Groups({"subject", "connectionType", "connection"})
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="CUSTOM")
	 * @ORM\CustomIdGenerator(class=UuidGenerator::class)
	 * @ORM\Column(type="uuid", unique=true)
	 */
	private string $id;

	/**
	 * @Groups({"subject", "connectionType", "connection"})
	 * @ORM\Column(type="string")
	 */
	private string $name;

	/**
	 * @ORM\OneToMany(targetEntity=Connection::class, mappedBy="connectionType")
	 */
	private Collection $connections;

	public function __construct()
	{
		$this->connections = new ArrayCollection();
	}

	public function getId(): ?string
	{
		return $this->id;
	}

	public function getName(): ?string
	{
		return $this->name;
	}

	public function setName(string $name): self
	{
		$this->name = $name;

		return $this;
	}

	/**
	 * @return Collection|Connection[]
	 */
	public function getConnections(): Collection
	{
		return $this->connections;
	}

	public function addConnection(Connection $connection): self
	{
		if (!$this->connections->contains($connection)) {
			$this->connections[] = $connection;
			$connection->setConnectionType($this);
		}

		return $this;
	}

	public function removeConnection(Connection $connection): self
	{
		if ($this->connections->contains($connection)) {
			$this->connections->removeElement($connection);
			// set the owning side to null (unless already changed)
			if ($connection->getConnectionType() === $this) {
				$connection->setConnectionType(null);
			}
		}

		return $this;
	}
}
