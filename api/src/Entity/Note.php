<?php

declare(strict_types=1);

namespace App\Entity;

use DateTime;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Doctrine\UuidGenerator;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity
 * @ORM\Table(name="notes")
 */
class Note
{
	/**
	 * @Groups({"subject"})
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="CUSTOM")
	 * @ORM\CustomIdGenerator(class=UuidGenerator::class)
	 * @ORM\Column(type="uuid", unique=true)
	 */
	private string $id;

	/**
	 * @Groups({"subject"})
	 * @ORM\Column(type="text")
	 */
	private string $note;

	/**
	 * @Groups({"subject"})
	 * @ORM\Column(type="datetime", options={"default": "CURRENT_TIMESTAMP"})
	 */
	private DateTimeInterface $createdAt;

	/**
	 * @ORM\ManyToOne(targetEntity=Subject::class, inversedBy="notes")
	 * @ORM\JoinColumn(nullable=false)
	 */
	private Subject $subject;

	public function __construct()
	{
		$this->createdAt = new DateTime();
	}

	public function getId(): ?string
	{
		return $this->id;
	}

	public function getSubject(): Subject
	{
		return $this->subject;
	}

	public function setSubject(Subject $subject): self
	{
		$this->subject = $subject;

		return $this;
	}

	public function getNote(): string
	{
		return $this->note;
	}

	public function setNote(string $note): self
	{
		$this->note = $note;

		return $this;
	}

	public function getCreatedAt(): ?DateTimeInterface
	{
		return $this->createdAt;
	}
}
