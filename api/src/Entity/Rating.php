<?php

declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Doctrine\UuidGenerator;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     normalizationContext={"groups"={"rating"}},
 *     graphql={
 *          "item_query",
 *          "delete",
 *          "update",
 *          "create"
 *     }
 * )
 *
 * @ORM\Entity
 * @ORM\Table(name="ratings")
 */
class Rating
{
	/**
	 * @Groups({"subject", "rating"})
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="CUSTOM")
	 * @ORM\CustomIdGenerator(class=UuidGenerator::class)
	 * @ORM\Column(type="uuid", unique=true)
	 */
	private string $id;

	/**
	 * @Groups({"subject", "rating"})
	 * @ORM\Column(type="string")
	 */
	private string $name;

	/**
	 * Rating Percentage as whole numbers
	 *
	 * @Groups({"subject", "rating"})
	 * @ORM\Column(type="smallint")
	 */
	private int $value;

	/**
	 * @ORM\ManyToOne(targetEntity=Subject::class, inversedBy="ratings")
	 * @ORM\JoinColumn(nullable=false)
	 */
	private Subject $subject;

	public function getId(): ?string
	{
		return $this->id;
	}

	public function getName(): ?string
	{
		return $this->name;
	}

	public function setName(string $name): self
	{
		$this->name = $name;

		return $this;
	}

	public function getValue(): ?int
	{
		return $this->value;
	}

	public function setValue(int $value): self
	{
		$this->value = $value;

		return $this;
	}

	public function getSubject(): ?Subject
	{
		return $this->subject;
	}

	public function setSubject(?Subject $subject): self
	{
		$this->subject = $subject;

		return $this;
	}
}
