<?php

declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Doctrine\UuidGenerator;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Subject Entity Class
 *
 * Main entity, representing any business subject to be categorized, tagged, rated, ...
 *
 * @ApiFilter(OrderFilter::class, properties={"id", "name", "createDateTime"})
 * @ApiFilter(
 *     SearchFilter::class,
 *     properties={
 *         "id": "exact",
 *         "name": "partial",
 *         "tags": "exact",
 *         "categories": "exact",
 *         "connectionOwners": "exact",
 *         "connectionTargets": "exact"
 *     }
 * )
 *
 * @ApiResource(
 *     normalizationContext={"groups"={"subject"}},
 *     attributes={
 *          "pagination_via_cursor"
 *     },
 *     graphql={
 *          "item_query",
 *          "collection_query"={
 *              "pagination_via_cursor"
 *          },
 *          "delete",
 *          "update",
 *          "create"
 *     }, collectionOperations={
 *          "get",
 *          "post",
 *     }, itemOperations={
 *          "get",
 *          "put",
 *          "delete",
 *     }
 * )
 * @ORM\Entity
 * @ORM\Table(name="subjects", indexes={
 *     @ORM\Index(name="search_idx", columns={"name"}),
 *     @ORM\Index(name="id_ix", columns={"id"})
 *  }
 * )
 */
class Subject
{
	/**
	 * @Groups({"subject", "connection"})
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="CUSTOM")
	 * @ORM\CustomIdGenerator(class=UuidGenerator::class)
	 * @ORM\Column(type="uuid", unique=true)
	 */
	private string $id;

	/**
	 * @Groups({"subject", "connection"})
	 * @ORM\Column(type="string")
	 */
	private string $name;

	/**
	 * @Groups({"subject", "connection"})
	 * @ORM\Column(type="datetime", options={"default": "CURRENT_TIMESTAMP"})
	 */
	private DateTimeInterface $createdAt;

	/**
	 * @Groups({"subject"})
	 * @ORM\ManyToMany(targetEntity=Category::class, inversedBy="subjects", fetch="EAGER")
	 */
	private Collection $categories;

	/**
	 * @Groups({"subject"})
	 * @ORM\OneToMany(targetEntity=Note::class, mappedBy="subject", fetch="EAGER")
	 */
	private Collection $notes;

	/**
	 * @Groups({"subject"})
	 * @ORM\ManyToMany(targetEntity=Tag::class, inversedBy="subjects", fetch="EAGER")
	 */
	private Collection $tags;

	/**
	 * @Groups({"subject"})
	 * @ORM\OneToMany(targetEntity=Rating::class, mappedBy="subject", orphanRemoval=true, fetch="EAGER")
	 */
	private Collection $ratings;

	/**
	 * @Groups({"subject"})
	 * @ORM\OneToMany(targetEntity=Connection::class, mappedBy="from", fetch="EAGER")
	 */
	private Collection $connectionOwners;

	/**
	 * @Groups({"subject"})
	 * @ORM\OneToMany(targetEntity=Connection::class, mappedBy="to", fetch="EAGER")
	 */
	private Collection $connectionTargets;

	public function __construct()
	{
		$this->createdAt = new \DateTimeImmutable();
		$this->categories = new ArrayCollection();
		$this->tags = new ArrayCollection();
		$this->ratings = new ArrayCollection();
		$this->connectionOwners = new ArrayCollection();
		$this->connectionTargets = new ArrayCollection();
	}

	public function getId(): string
	{
		return $this->id;
	}

	public function setId(string $id): string
	{
		$this->id = $id;
	}

	public function getName(): string
	{
		return $this->name;
	}

	public function setName(string $name): void
	{
		$this->name = $name;
	}

	public function getCreatedAt(): ?DateTimeInterface
	{
		return $this->createdAt;
	}

	/**
	* @return Collection|Category[]
	*/
	public function getCategories(): Collection
	{
		return $this->categories;
	}

	public function addCategory(Category $category): self
	{
		if (!$this->categories->contains($category)) {
			 $this->categories->add($category);
			 $category->addSubject($this);
		}

		return $this;
	}

	public function removeCategory(Category $category): self
	{
		if ($this->categories->contains($category)) {
			$this->categories->removeElement($category);
			$category->removeSubject($this);
		}

		 return $this;
	}

	public function getNotes()
	{
		return $this->notes;
	}

	public function setNotes($notes): void
	{
		$this->notes = $notes;
	}

	/**
	 * @return Collection|Tag[]
	 */
	public function getTags(): Collection
	{
		return $this->tags;
	}

	public function addTag(Tag $tag): self
	{
		if (!$this->tags->contains($tag)) {
			$this->tags->add($tag);
			$tag->addSubject($this);
		}

		return $this;
	}

	public function removeTag(Tag $tag): self
	{
		if ($this->tags->contains($tag)) {
			$this->tags->removeElement($tag);
			$tag->removeSubject($this);
		}

		return $this;
	}

	public function getRatings()
	{
		return $this->ratings;
	}

	public function setRatings($ratings): void
	{
		$this->ratings = $ratings;
	}

	public function getConnectionOwners()
	{
		return $this->connectionOwners;
	}

	public function setConnectionOwners($owners): void
	{
		$this->connectionOwners = $owners;
	}

	public function getConnectionTargets()
	{
		return $this->connectionTargets;
	}

	public function setConnectionTargets($targets): void
	{
		$this->connectionTargets = $targets;
	}
}
