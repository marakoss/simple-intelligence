<?php

declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Doctrine\UuidGenerator;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 *
 * Tag Entity Class
 *
 * Represents labels in client application
 *
 * @ApiResource(
 *     normalizationContext={"groups"={"tag"}},
 *     attributes={
 *          "pagination_via_cursor"
 *     },
 *     graphql={
 *          "item_query",
 *          "collection_query"={
 *              "pagination_via_cursor"
 *          },
 *          "delete",
 *          "update",
 *          "create"
 *     }
 * )
 * @ORM\Entity
 * @ORM\Table(name="tags")
 */
class Tag
{
	/**
	 * @Groups({"subject", "tag"})
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="CUSTOM")
	 * @ORM\CustomIdGenerator(class=UuidGenerator::class)
	 * @ORM\Column(type="uuid", unique=true)
	 */
	private string $id;

	/**
	 * @Groups({"subject", "tag"})
	 * @ORM\Column(type="string")
	 */
	private string $name;

	/**
	 * @ORM\ManyToMany(targetEntity=Subject::class, mappedBy="tags")
	 */
	private Collection $subjects;


	public function __construct()
	{
		$this->subjects = new ArrayCollection();
	}

	public function getId(): ?string
	{
		return $this->id;
	}

	public function getName(): ?string
	{
		return $this->name;
	}

	public function setName(string $name): self
	{
		$this->name = $name;

		return $this;
	}

	/**
	 * @return Collection|Subject[]
	 */
	public function getSubjects(): Collection
	{
		return $this->subjects;
	}

	public function addSubject(Subject $subject): self
	{
		if (!$this->subjects->contains($subject)) {
			$this->subjects[] = $subject;
			$subject->addTag($this);
		}

		return $this;
	}

	public function removeSubject(Subject $subject): self
	{
		if ($this->subjects->contains($subject)) {
			$this->subjects->removeElement($subject);
			$subject->removeTag($this);
		}

		return $this;
	}
}
